all: josefo

josefo: josefo.c josefo_est.o
	gcc -g -o josefo josefo.c josefo_est.o
josefo_est.o: josefo_est.c
	gcc -g -c josefo_est.c

clean:
	rm -Rf *.o
	rm teste

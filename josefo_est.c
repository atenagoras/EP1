#include "josefo_est.h"
#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<time.h>

/* criaNo(): cria nó a partir da string dada
   Parâmetros:
   char *nome: Nome do soldado a ser inserido
   Retorno: (No *) Endereço do nó criado */
No* criaNo(char *nome){
  No* soldado = (No *) malloc(sizeof(No) );
  if (soldado == NULL)
    return NULL;
  strcpy(soldado->Nome, nome);
  soldado->flag = 0;
  soldado->prox = NULL;
  soldado->ant = NULL;
  return soldado;
}

/*
void excluiNo(No *soldado){
  free(soldado);
}
*/
void criaLista(Lista *l){
  l->ini = NULL;
  l->fim = NULL;
  l->N = 0;
}

//==================================================================


int excluiNo(Lista *l, No *soldado){
  No* i;
  i = busca_seq(l, soldado);
  if(i == NULL) return 0;

  free(i);
  return 1;
}

//==================================================================

void excluiLista(Lista *l){
  No *q = l->ini->prox;
  int i;
  do{
    i = excluiNo(l, q);
    q = q->prox;
  }while(q != l->ini);
  l->ini = NULL;
  l->fim = NULL;
  l->N = 0;
}

/*
void excluiLista(Lista *l){
  No *p = l->ini->prox;
  while(p != l->fim->prox){
    excluiNo(l->ini);
    l->ini = p;
    p = p->prox;
  }
  free(l->fim);
  l->ini = NULL;
  l->fim = NULL;
  l->N = 0;
}
*/

int sorteia(int s){
  int k;
  srand(time(NULL));
  k = 1 + (rand() % s) * time(NULL);
  srand(rand()*time(NULL)*rand() / (1+rand() ) + (k*(rand()%s) / s) * (s - 1) * (s - 2) * (s - 3) * (k - s) );
  k = 1 + (rand() % s);
  return k;
}

No *removeSoldado(Lista *l, No* soldado){
  No *Soldanterior, *Soldproximo; // ponteiros auxiliares
	Soldanterior = soldado->ant; //recebendo soldado anterior ao 'soldado'. 
	Soldproximo = soldado->prox; //recebendo soldado posterior ao 'soldado' fica no meio)
	Soldanterior->prox = Soldproximo; //soldado anterior ao 'soldado' recebe como posterior o soldado posterior ao 'soldado'
	Soldproximo->ant = Soldanterior; //soldado posterior ao 'soldado' recebe como anterior o soldado anterior ao 'soldado'
	l->N--;
	if (l->ini == soldado)
	  l->ini = l->ini->prox;
	if (l->fim == soldado)
	  l->fim = l->fim->ant;
	soldado->prox = NULL;
	soldado->ant = NULL;
	return soldado;
}

/*reinserir soldado:
  Se a flag do soldado é menor que dois, ele pode retornar para a lista, se não for, ele fica onde está e não recebe ações.
*/
int reinserirSoldado(Lista *princ, Lista *derrot, No *abatido){
  No* resgatado;
  if(abatido->flag < 2){
    resgatado =  removeSoldado(derrot, abatido);
    insere(princ, NULL, 'o', resgatado);
    return 1;
  }
  return 0;
}


//==================================================================

/*abateSoldado: Remove o Nó(soldado) da lista sem apagá-lo. Idéia é jogá-lo em uma lista de soldados derrotados
  Parâmetros
  Lista *l: Lista de onde o soldado deverá ser removido
  No *abatido: Soldado a ser removido
  Retorno: (No*) Soldado removido */

No* abateSoldado(Lista *l, No *abatido){
  No *Soldanterior, *Soldproximo; // ponteiros auxiliares
	Soldanterior = abatido->ant; //recebendo soldado anterior ao abatido. 
	Soldproximo = abatido->prox; //recebendo soldado posterior ao abatido (soldado abatido fica no meio)
	Soldanterior->prox = Soldproximo; //soldado anterior ao abatido recebe como posterior o soldado posterior ao abatido
	Soldproximo->ant = Soldanterior; //soldado posterior ao abatido recebe como anterior o soldado anterior ao abatido
	l->N--;
	if (l->ini == abatido)
	  l->ini = l->ini->prox;
	if (l->fim == abatido)
	  l->fim = l->fim->ant;
	abatido->flag++;
	abatido->prox = NULL;
	abatido->ant = NULL;
	return abatido;	
}

int verificaVazio(Lista *l){
	if(l->ini == NULL && l->fim == NULL) return 1;
	else return 0; 
}//verifica se a lista está vazia, ou seja, se o inicio e o fim é NULL.

No* busca_seq(Lista *l, No* soldado){
  if (verificaVazio(l))
    return NULL;
  No *p = l->ini;
  int t_no, t_nome; // testes de comparação de nós e de nomes, respectivamente
  do {
    t_no = p == soldado;
    t_nome = strcmp(p->Nome, soldado->Nome);
    p->prox;
  } while (p != l->ini && t_no != 0 && t_nome != 0);
  if (p == l->ini)
    return NULL; // não encontrou
  if (t_nome == 0)
    return soldado; // soldado já existe
  return p; // nome de soldado já existe, não é necessário inserir novo nó
}

//INSERIR SEM ORDENAR===============================================
void insere_normal(Lista *l, No *soldado){
  if(verificaVazio(l)){
    l->ini = soldado;
    l->fim = soldado;
    soldado->ant = soldado;
    soldado->prox = soldado;
  }	
  else{
    soldado->ant = l->fim;
    soldado->prox = l->ini;
    l->fim->prox = soldado;
    l->ini->ant = soldado;
    l->fim = soldado;
  }
  l->N++;
}	


void insere_antes(Lista *l, No *antes, No *soldado){
  soldado->prox = antes;// antes virou depois
  soldado->ant = antes->ant;// anterior ao antes, agora é anterior ao soldado
  antes->ant->prox = soldado;// próximo do anterior (que era o próprio antes) agora é o soldado
  antes->ant = soldado;// como antes já está depois do soldado, o anterior ao antes, é o soldado
  if (l->N == 1){// antes da inserção ser contabilizada (o será depois de sair desta função)
    l->ini = soldado;
    l->fim = antes;
  }
  if (antes == l->ini)
    l->ini = soldado;  
}

void insere_depois(Lista *l, No *soldado, No *depois){
  soldado->ant = depois;// depois virou antes
  soldado->prox = depois->prox; // próximo do depois agora é próximo do soldado
  depois->prox->ant = soldado;// anterior do próximo (que era o própio depois) agora é o soldado
  depois->prox = soldado;// como depois já é anterior ao soldado, o próximo do depois é o soldado
  if (l->N == 1){// antes da inserção ser contabilizada (o será depois de sair desta função)
    l->ini = depois;
    l->fim = soldado;
  }
  if (depois == l->fim)
    l->fim = soldado;
}

int insere_ord(Lista *l, No *soldado){
  if (verificaVazio(l)){ // se for a primeira inserção, não é necessário comparação
    insere_normal(l, soldado);
    return 1;
  }
  No *p = l->ini;
  int t; // teste
  // Enquanto soldado->Nome não for menor que o p->Nome, ou p = l->fim, não insere-o
  while( (t = strcmp(p->Nome, soldado->Nome) ) < 0 && p->prox != l->ini )
    p = p->prox;
  if (t > 0)// soldado->Nome < p->Nome
    insere_antes(l, p, soldado);
  else if (t < 0) // soldado->Nome > p->Nome // Será inserido no fim (aqui, provavelmene p == l->fim
    insere_depois(l, soldado, p);
  else{
    printf("Já existe este soldado na lista!\n");
    return 0;
  }
  l->N++;
  return 1;
}


/* insere(): Insere nome em nó, depois insere na lista. Há duas formas:
   n: faz uma inserção normal no "fim" da fila circular
   o: faz uma inserção ordenada na lista circular
   OU Insere No* No, se este não for nulo
   Parâmetros:
   Lista *l   : Lista a ter o soldado inserido
   char *nome : Nome do soldado a ser inserido
        *forma: forma de inserção: 'n': inserção normal
	                           'o': inserção ordenada
   No *NO     : Nó a ser inserido caso não seja nulo
   Retorno: (int): 0 - falha
                   1 - sucesso */
int insere(Lista *l, char *nome, char forma, No *NO){
  No* soldado;
  if (NO == NULL)
    soldado = criaNo(nome);
  else soldado = NO;
  if (soldado == NULL) // falhou a criação do novo nó
    return 0;
  if (forma == 'n'){
    if (busca_seq(l, soldado) != NULL){
      printf("Nó com o nome do soldado já existe!\nEste não será incluido!\n");
      if (NO == NULL)
	free(soldado);
    return 0;
    }
    insere_normal(l, soldado);
  }
  else if (forma == 'o')
    insere_ord(l, soldado);
  else{
    free(soldado);
    printf("Não inseriu.\nInsira 'n' ou 'o' no último parâmetro!\n");
    return 0;
  }
  return 1;
}

/*MUDANÇAS DE HOJE================================================*/

void imprime_lista(Lista *l){ // não sei se está correto
  /* No *aux = l->ini;
  int i = 0;
  do{
    printf("%s\n", aux);
    aux = aux->prox;
    i++;
  }while(aux != l->ini && i < l->N); */
    No* p = l->ini;
    do {
    printf("%s\n", p->Nome);
    p = p->prox;
  } while (p != l->ini);
  
}

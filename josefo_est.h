typedef struct no{
  char Nome[256];
  int flag; // para marcar o soldado eliminado
  struct no *ant, *prox;
} No;

typedef struct{
  No *ini, *fim;
  int N; // para gerenciar o tamanho da lista
} Lista;

// Manipulação dos Nós
No *criaNo(char *nome);
int excluiNo(Lista *l, No *soldado);

// Manipulação da Lista
void criaLista(Lista* l); //ok
int insere_ord(Lista *l, No* soldado); //ok
No* abateSoldado(Lista* l, No* abatido); //ok
void excluiLista(Lista *l); //ok
No *busca_seq(Lista *l, No *soldado); //ok
void insere_normal(Lista *l, No* soldado); //ok
int sorteia(int s); //ok
int insere(Lista *l, char *nome, char forma, No* NO);
No *removeSoldado(Lista* l, No* soldado);
int reinserirSoldado(Lista *princ, Lista *derrot, No *abatido);
void imprime_lista(Lista *l);

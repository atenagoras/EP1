#include<stdio.h>
#include"josefo_est.h"
#include<stdlib.h>
#include<string.h>

/* O problema de Josefo:
   Nomes: Atenágoras Souza Silva nº USP:5447262
          Priscilla 11366340 nº USP: 11366340
   Profª Karla Silva - Disciplina: Algoritmos e Estrutura de Dados
*/

No* sortearNome(Lista *l){
  No *Ini, *aux = l->ini;
  int N = l->N, n, i;
  n = sorteia(N);
  for (i = 0; i < n; i++)
    aux = aux->prox;
  printf("Número sorteado %i\n", n);
  printf("Nome sorteado: %s\n", aux);
  return aux;
}

void loopJosefo (Lista *princ, Lista *derrot, int t, int Npassos) {
  No *nomeInicial, *aux=princ->ini, *abatido, *regressante;
  int i, j = 0, passos;
  do{
    nomeInicial = sortearNome(princ); //vai retornar um nome
    passos = sorteia(Npassos); //sorteia um numero k de passos
    printf("Número de passos sorteado: %i\n", passos);
    aux = nomeInicial; // este é o nó de origem para sortear...
    for ( i = 0; i <= passos; i++) aux = aux->prox;
    printf("Soldado eliminado: %s\n", aux);
    abatido = abateSoldado(princ, aux);//retira soldado da lista principal e atualiza os ponteiros
    insere(derrot, NULL, 'o', abatido); //insere o soldado abatido na lista de abatidos
    if(j == t){
      regressante = sortearNome(derrot);
      printf("Soldado regressante:%s\n", regressante);
      reinserirSoldado(princ, derrot, regressante);
      j = 0;
    }
    j++;
  }while(princ->N > 1); // sai efetivamente do loop
  // imprime resultado final
  printf("Soldados abatidos: \n");
  imprime_lista(derrot);
  printf("Soldado sobrevivente: \n");
  imprime_lista(princ);
}

int main(){
  Lista principal;
  Lista derrotados;
  criaLista(&principal);
  criaLista(&derrotados);
  char soldado[256];
  int t, i, quantSold;
  printf("Quantos soldados deseja colocar?\n");
  scanf("%i\n", &quantSold);
  for(i = 0; i < quantSold; i++){
    printf("Nome do soldado: \n");
    scanf("%[^\n]\n", soldado);
    insere(&principal, soldado, 'o', NULL);
  }
  printf("Insira o valor t (inteiro maior que 0):\n");
  scanf("%i\n", &t);
  while(t <= 0){
    printf("Valor inválido! Digite novamente: \n");
    scanf("%i\n", t);
  }
  imprime_lista(&principal);
  loopJosefo(&principal, &derrotados, t, quantSold);
  excluiLista(&principal);
  excluiLista(&derrotados);
}

#include<stdio.h>
#include<stdlib.h>
#include "josefo_est.h"

void printLista(Lista L){
  No* p = L.ini;
  do {
    printf("%s\n", p->Nome);
    p = p->prox;
  } while (p != L.ini);
}

int main(){
  Lista L;
  criaLista(&L);
  char Linha[256];
  int i, N = 10;
  for (i = 0; i < N; i++){
    scanf("%[^\n]\n", Linha);
    printf("%s\n", Linha);
    insere(&L, Linha, 'o', NULL);
    printf("%d\n", L.N);
  }
  printLista(L);
  excluiLista(&L);
}
